<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle\Client;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Command\CommandInterface;
use GuzzleHttp\Command\Guzzle\DescriptionInterface;
use GuzzleHttp\Command\Guzzle\GuzzleClient as BaseGuzzleClient;
use Nfq\Bundle\GuzzleConfigBundle\Transformer\ResponseTransformersChainInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class GuzzleClient extends BaseGuzzleClient
{
    /**
     * @var ResponseTransformersChainInterface
     */
    protected $responseTransformersChain;

    /**
     * @var DescriptionInterface
     */
    protected $description;

    /**
     * @param ClientInterface $client
     * @param DescriptionInterface $description
     * @param ResponseTransformersChainInterface $responseTransformersChain
     * @param array $config
     */
    public function __construct(
        ClientInterface $client,
        DescriptionInterface $description,
        ResponseTransformersChainInterface $responseTransformersChain,
        array $config = []
    ) {
        $this->responseTransformersChain = $responseTransformersChain;
        $this->description = $description;

        parent::__construct(
            $client,
            $description,
            null,
            [$this, 'transformResponse'],
            null,
            $config
        );
    }

    /**
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @param CommandInterface $command
     * @return mixed
     */
    public function transformResponse(
        ResponseInterface $response,
        RequestInterface $request,
        CommandInterface $command
    ) {
        return $this->responseTransformersChain->transformResponse(
            $response,
            $request,
            $command,
            $this->description
        );
    }
}
