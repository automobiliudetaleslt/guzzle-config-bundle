<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle\Transformer;

use GuzzleHttp\Command\CommandInterface;
use GuzzleHttp\Command\Guzzle\DescriptionInterface;
use GuzzleHttp\Command\Guzzle\Deserializer;
use Nfq\Bundle\GuzzleConfigBundle\Exception\ResponseTransformerNotFoundException;
use Nfq\Bundle\GuzzleConfigBundle\Factory\GuzzleDeserializerFactory;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ResponseTransformersChain implements ResponseTransformersChainInterface
{
    /**
     * @var ResponseTransformerInterface[]
     */
    protected $responseTransformers = [];

    /**
     * @var GuzzleDeserializerFactory
     */
    protected $guzzleDeserializerFactory;

    /**
     * @param GuzzleDeserializerFactory $guzzleDeserializerFactory
     */
    public function __construct(GuzzleDeserializerFactory $guzzleDeserializerFactory)
    {
        $this->guzzleDeserializerFactory = $guzzleDeserializerFactory;
    }

    /**
     * @param ResponseTransformerInterface $responseTransformer
     */
    public function addResponseTransformer(ResponseTransformerInterface $responseTransformer)
    {
        if (!\in_array($responseTransformer, $this->responseTransformers, true)) {
            $this->responseTransformers[] = $responseTransformer;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function transformResponse(
        ResponseInterface $response,
        RequestInterface $request,
        CommandInterface $command,
        DescriptionInterface $description
    ) {
        if (!$this->doResponseTransformation($description, $command)) {
            return $response;
        }

        if (null === $transformerClass = $this->getTransformerClass($description, $command)) {
            return $response;
        }

        foreach ($this->responseTransformers as $responseTransformer) {
            if ($responseTransformer instanceof $transformerClass) {
                return $responseTransformer->transformResponse($response, $request, $command, $description);
            }
        }

        if (Deserializer::class === $transformerClass) {
            return $this->guzzleDeserializerFactory->create($description)->__invoke($response, $request, $command);
        }

        throw new ResponseTransformerNotFoundException(
            $response,
            \sprintf('"%s" response transformer is not loaded to "%s"', $transformerClass, self::class)
        );
    }

    /**
     * @param DescriptionInterface $description
     * @param CommandInterface $command
     * @return null|string
     */
    protected function getTransformerClass(DescriptionInterface $description, CommandInterface $command): ?string
    {
        $operation = $description->getOperation($command->getName());
        $transformerClass = $operation->getData('transformerClass');

        return null !== $transformerClass ? $transformerClass : $description->getData('transformerClass');
    }

    /**
     * @param DescriptionInterface $description
     * @param CommandInterface $command
     * @return bool
     */
    protected function doResponseTransformation(DescriptionInterface $description, CommandInterface $command): bool
    {
        $operation = $description->getOperation($command->getName());
        $transformResponse = $operation->getData('transformResponse');

        return null === $transformResponse || true === $transformResponse;
    }
}
