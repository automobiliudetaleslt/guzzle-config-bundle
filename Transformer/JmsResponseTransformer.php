<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle\Transformer;

use GuzzleHttp\Command\CommandInterface;
use GuzzleHttp\Command\Guzzle\DescriptionInterface;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class JmsResponseTransformer implements ResponseTransformerInterface
{
    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public function transformResponse(
        ResponseInterface $response,
        RequestInterface $request,
        CommandInterface $command,
        DescriptionInterface $description
    ) {
        return $this->serializer->deserialize(
            (string)$response->getBody(),
            $this->getModelName($command, $description),
            $this->getResponseContentType($response),
            $this->getDeserializationContext($description, $command)
        );
    }

    /**
     * @param CommandInterface $command
     * @param DescriptionInterface $description
     * @return string
     */
    protected function getModelName(CommandInterface $command, DescriptionInterface $description): string
    {
        /** @var string|null $responseModel */
        $responseModel = $description->getOperation($command->getName())->getResponseModel();

        return null !== $responseModel ? $responseModel : 'array';
    }

    /**
     * @param ResponseInterface $response
     * @return string
     */
    protected function getResponseContentType(ResponseInterface $response): string
    {
        $contentTypeHeaders = (array)$response->getHeader('Content-Type');

        foreach ($contentTypeHeaders as $contentTypeHeader) {
            if (false !== \stripos($contentTypeHeader, 'json')) {
                return 'json';
            }
            if (false !== \stripos($contentTypeHeader, 'xml')) {
                return 'xml';
            }
        }

        throw new \RuntimeException(
            \sprintf(
                "'%s' content type decoding is not implemented in %s\nResponse body: '%s'",
                \implode(', ', $contentTypeHeaders),
                static::class,
                (string)$response->getBody()
            )
        );
    }

    /**
     * @param DescriptionInterface $description
     * @param CommandInterface $command
     * @return DeserializationContext|null
     */
    protected function getDeserializationContext(
        DescriptionInterface $description,
        CommandInterface $command
    ) {
        $operation = $description->getOperation($command->getName());

        if (null === $operation->getData('jms_serializer.groups')
            && null === $operation->getData('jms_serializer.version')
            && null === $operation->getData('jms_serializer.max_depth_checks')
        ) {
            return null;
        }

        $context = DeserializationContext::create();

        if (null !== $groups = $operation->getData('jms_serializer.groups')) {
            $context->setGroups($groups);
        }

        if (null !== $version = $operation->getData('jms_serializer.version')) {
            $context->setVersion($version);
        }

        if (true === $operation->getData('jms_serializer.max_depth_checks')) {
            $context->enableMaxDepthChecks();
        }

        return $context;
    }
}
