# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.0] - 2019-03-26
### Added XML support to `Nfq\Bundle\GuzzleConfigBundle\Transformer\JmsResponseTransformer`

## [1.0.0] - 2019-02-15
### Initial release
