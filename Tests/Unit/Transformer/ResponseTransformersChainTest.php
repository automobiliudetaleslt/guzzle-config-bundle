<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Tests\Unit\Transformer;

use GuzzleHttp\Command\CommandInterface;
use GuzzleHttp\Command\Guzzle\DescriptionInterface;
use GuzzleHttp\Command\Guzzle\Deserializer;
use GuzzleHttp\Command\Guzzle\Operation;
use Nfq\Bundle\GuzzleConfigBundle\Factory\GuzzleDeserializerFactory;
use Nfq\Bundle\GuzzleConfigBundle\Transformer\ResponseTransformerInterface;
use Nfq\Bundle\GuzzleConfigBundle\Transformer\ResponseTransformersChain;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ResponseTransformersChainTest extends TestCase
{
    /**
     * @var ResponseTransformersChain
     */
    private $transformersChain;

    /**
     * @var GuzzleDeserializerFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $guzzleDeserializerFactory;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->guzzleDeserializerFactory = $this->createMock(GuzzleDeserializerFactory::class);
        $this->transformersChain = new ResponseTransformersChain($this->guzzleDeserializerFactory);

        parent::setUp();
    }

    /**
     * @test
     */
    public function testTransformResponseNoTransformerClass()
    {
        $this->guzzleDeserializerFactory->expects($this->never())->method('create');

        $response = $this->getResponse();

        $this->assertSame(
            $response,
            $this->transformersChain->transformResponse(
                $response,
                $this->getRequest(),
                $this->getCommand(),
                $this->getDescription(null)
            )
        );
    }

    /**
     * @test
     */
    public function testTransformResponseWithTransformerClass()
    {
        $this->guzzleDeserializerFactory->expects($this->never())->method('create');

        $responseTransformer = $this->getResponseTransformer();
        $responseTransformer->method('transformResponse')->willReturn(['response']);

        $this->transformersChain->addResponseTransformer($responseTransformer);

        $this->assertSame(
            ['response'],
            $this->transformersChain->transformResponse(
                $this->getResponse(),
                $this->getRequest(),
                $this->getCommand(),
                $this->getDescription(ResponseTransformerInterface::class)
            )
        );
    }

    /**
     * @test
     */
    public function testTransformResponseWithDefaultDeserializer()
    {
        $description = $this->getDescription(Deserializer::class);

        $deserializer = $this->createMock(Deserializer::class);
        $deserializer->method('__invoke')->willReturn(['deserializer response']);

        $this->guzzleDeserializerFactory->method('create')->with($description)->willReturn($deserializer);

        $this->assertSame(
            ['deserializer response'],
            $this->transformersChain->transformResponse(
                $this->getResponse(),
                $this->getRequest(),
                $this->getCommand(),
                $description
            )
        );
    }

    /**
     * @test
     * @expectedException \Nfq\Bundle\GuzzleConfigBundle\Exception\ResponseTransformerNotFoundException
     */
    public function testTransformResponseTransformerNotFoundException()
    {
        $this->guzzleDeserializerFactory->expects($this->never())->method('create');

        $this->transformersChain->transformResponse(
            $this->getResponse(),
            $this->getRequest(),
            $this->getCommand(),
            $this->getDescription('TestResponseTransformer')
        );
    }

    /**
     * @return ResponseInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected function getResponse()
    {
        return $this->createMock(ResponseInterface::class);
    }

    /**
     * @return RequestInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected function getRequest()
    {
        return $this->createMock(RequestInterface::class);
    }

    /**
     * @return CommandInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected function getCommand()
    {
        return $this->createMock(CommandInterface::class);
    }

    /**
     * @param null|string $transformerClass
     * @return DescriptionInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected function getDescription(string $transformerClass = null)
    {
        $operationMock = $this->createMock(Operation::class);
        $operationMock->expects($this->at(0))->method('getData')->with('transformResponse')->willReturn(null);
        $operationMock->expects($this->at(1))->method('getData')->with('transformerClass')->willReturn(null);

        $description = $this->createMock(DescriptionInterface::class);
        $description->method('getData')->with('transformerClass')->willReturn($transformerClass);
        $description->method('getOperation')->willReturn($operationMock);

        return $description;
    }

    /**
     * @return ResponseTransformerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected function getResponseTransformer()
    {
        return $this->createMock(ResponseTransformerInterface::class);
    }
}
