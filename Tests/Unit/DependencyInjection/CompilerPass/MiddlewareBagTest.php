<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Tests\Unit\DependencyInjection\CompilerPass;

use Nfq\Bundle\GuzzleConfigBundle\DependencyInjection\CompilerPass\MiddlewareBag;
use PHPUnit\Framework\TestCase;

class MiddlewareBagTest extends TestCase
{
    /**
     * @test
     */
    public function testBag()
    {
        $bag = new MiddlewareBag();
        $bag->add('middleware1', 'app_middleware1', false, 500);
        $bag->add('middleware2', 'app_middleware2', true, 0);
        $bag->add('middleware3', 'app_middleware3', true, 800);
        $bag->add('middleware4', 'app_middleware4', false, -1);

        $this->assertCount(4, $bag);

        $this->assertSame(
            [
                [
                    'id' => 'middleware3',
                    'alias' => 'app_middleware3',
                    'global' => true,
                    'priority' => 800,
                ],
                [
                    'id' => 'middleware1',
                    'alias' => 'app_middleware1',
                    'global' => false,
                    'priority' => 500,
                ],
                [
                    'id' => 'middleware2',
                    'alias' => 'app_middleware2',
                    'global' => true,
                    'priority' => 0,
                ],
                [
                    'id' => 'middleware4',
                    'alias' => 'app_middleware4',
                    'global' => false,
                    'priority' => -1,
                ],
            ],
            $bag->all()
        );

        $this->assertSame(
            [
                [
                    'id' => 'middleware3',
                    'alias' => 'app_middleware3',
                    'global' => true,
                    'priority' => 800,
                ],
                [
                    'id' => 'middleware2',
                    'alias' => 'app_middleware2',
                    'global' => true,
                    'priority' => 0,
                ],
            ],
            $bag->getGlobalMiddlewares()
        );

        $this->assertSame(
            [
                [
                    'id' => 'middleware1',
                    'alias' => 'app_middleware1',
                    'global' => false,
                    'priority' => 500,
                ],
                [
                    'id' => 'middleware4',
                    'alias' => 'app_middleware4',
                    'global' => false,
                    'priority' => -1,
                ],
            ],
            $bag->getNonGlobalMiddlewares()
        );

        $filtered = $bag->filterByWhiteBlackList([], []);
        $this->assertInstanceOf(MiddlewareBag::class, $filtered);
        $this->assertSame($filtered->all(), $bag->getGlobalMiddlewares());

        $filtered = $bag->filterByWhiteBlackList(['app_middleware1'], ['app_middleware2']);
        $this->assertSame(
            [
                [
                    'id' => 'middleware3',
                    'alias' => 'app_middleware3',
                    'global' => true,
                    'priority' => 800,
                ],
                [
                    'id' => 'middleware1',
                    'alias' => 'app_middleware1',
                    'global' => false,
                    'priority' => 500,
                ],
            ],
            $filtered->all()
        );

        try {
            $bag->filterByWhiteBlackList(['app_middleware1'], ['app_middleware4']);
            $this->fail('LogicException should be thrown when trying to blacklist non global middleware');
        } catch (\LogicException $e) {
            $this->assertTrue(true);
        }
    }
}
