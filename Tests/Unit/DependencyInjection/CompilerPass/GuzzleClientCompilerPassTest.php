<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Tests\Unit\DependencyInjection\CompilerPass;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractCompilerPassTestCase;
use Nfq\Bundle\GuzzleConfigBundle\DependencyInjection\CompilerPass\GuzzleClientCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class GuzzleClientCompilerPassTest extends AbstractCompilerPassTestCase
{
    /**
     * @test
     */
    public function testProcessSuccess()
    {
        $clientDefinition = new Definition(\GuzzleHttp\Client::class);
        $clientDefinition->addTag('nfq_guzzle_config.client', ['alias' => 'foo']);
        $this->setDefinition('app.client.foo', $clientDefinition);

        $this->compile();

        $this->assertContainerBuilderHasService('nfq_guzzle_config.client.foo');
        $this->assertContainerBuilderHasService('nfq_guzzle_config.client.foo.manager');
    }

    /**
     * @test
     * @expectedException \RuntimeException
     */
    public function testProcessDuplicateAliasException()
    {
        $clientDefinition = new Definition(\GuzzleHttp\Client::class);
        $clientDefinition->addTag('nfq_guzzle_config.client', ['alias' => 'foo']);
        $this->setDefinition('app.client.foo', $clientDefinition);
        $this->setDefinition('app.client.bar', $clientDefinition);

        $this->compile();
    }

    /**
     * {@inheritdoc}
     */
    protected function registerCompilerPass(ContainerBuilder $container)
    {
        $container->addCompilerPass(new GuzzleClientCompilerPass());
    }
}
