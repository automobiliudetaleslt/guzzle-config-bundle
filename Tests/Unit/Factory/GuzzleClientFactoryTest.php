<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Tests\Unit\Factory;

use GuzzleHttp\ClientInterface;
use Nfq\Bundle\GuzzleConfigBundle\Client\GuzzleClient;
use Nfq\Bundle\GuzzleConfigBundle\Factory\GuzzleClientFactory;
use Nfq\Bundle\GuzzleConfigBundle\Transformer\ResponseTransformersChainInterface;
use PHPUnit\Framework\TestCase;

class GuzzleClientFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function testGetClient()
    {
        $client = $this->createMock(ClientInterface::class);
        $client->method('getConfig')->willReturn([]);

        $responseTransformersChain = $this->createMock(ResponseTransformersChainInterface::class);

        $factory = new GuzzleClientFactory($responseTransformersChain);

        $this->assertInstanceOf(
            GuzzleClient::class,
            $factory->getClient($client)
        );
    }
}
