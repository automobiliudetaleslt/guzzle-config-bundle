<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Tests\Unit\Factory;

use GuzzleHttp\Command\Guzzle\DescriptionInterface;
use GuzzleHttp\Command\Guzzle\Deserializer;
use Nfq\Bundle\GuzzleConfigBundle\Factory\GuzzleDeserializerFactory;
use PHPUnit\Framework\TestCase;

class GuzzleDeserializerFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function testCreate()
    {
        $this->assertInstanceOf(
            Deserializer::class,
            (new GuzzleDeserializerFactory())->create($this->createMock(DescriptionInterface::class))
        );
    }
}
