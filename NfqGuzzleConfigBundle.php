<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle;

use Nfq\Bundle\GuzzleConfigBundle\DependencyInjection\CompilerPass\GuzzleClientCompilerPass;
use Nfq\Bundle\GuzzleConfigBundle\DependencyInjection\CompilerPass\JmsResponseTransformerPass;
use Nfq\Bundle\GuzzleConfigBundle\DependencyInjection\CompilerPass\MiddlewarePass;
use Nfq\Bundle\GuzzleConfigBundle\DependencyInjection\CompilerPass\ResponseTransformerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class NfqGuzzleConfigBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new MiddlewarePass());
        $container->addCompilerPass(new JmsResponseTransformerPass());
        $container->addCompilerPass(new ResponseTransformerPass());
        $container->addCompilerPass(new GuzzleClientCompilerPass());
    }
}
