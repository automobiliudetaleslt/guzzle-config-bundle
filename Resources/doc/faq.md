FAQ
===

How to run tests?
-----------------

```bash
php Tests/app/console.php cache:clear
# Coding standard
vendor/bin/phpcs
# Static analysis
vendor/bin/phpstan analyse
# Unit and functional tests
php Tests/app/console.php server:start 0.0.0.0:8000 --docroot=Tests/app/
vendor/bin/phpunit
php Tests/app/console.php server:stop
```

---

Back to: [Middleware](middleware.md)
