Configuration and Usage
=======================

Define your client
------------------

The tag line is important, and requires both the `name: nfq_guzzle_config.client` and `alias` parts.

```yaml
# services.yml

services:
    app.client.foo:
        class: GuzzleHttp\Client
        tags:
            - { name: nfq_guzzle_config.client, alias: foo }
        arguments:
            -
                baseUrl: "http://foo"
                headers:
                    Content-Type: 'application/json'
                    Accept: 'application/json'
                timeout: 20
                connect_timeout: 20
                operations:
                    readBar:
                        httpMethod: "GET"
                        uri: "/bar/{barId}"
                        parameters:
                            barId:
                                type: "string"
                                location: "uri"
                                required: true
                            title:
                                type: "string"
                                location: "json"
                                required: false
                    # other operations here    
      
```
[Guzzle services documentation](http://guzzle3.readthedocs.io/webservice-client/guzzle-service-descriptions.html)

Use the client
--------------

A new client service will appear, called `nfq_guzzle_config.client.{the alias you used in service tag}`.
You can call the operations directly:

```php
   $response = $this->get('nfq_guzzle_config.client.foo')->readBar(['barId' => 1, 'title' => 'baz']);
```

---

Back to: [Installation](install.md)

Next section: [Usage with JMS response transformer](configuration_and_usage_jms.md)
