Middleware
==========

Creating a middleware
---------------------

Creating a new Guzzle [middleware](http://guzzle.readthedocs.org/en/latest/handlers-and-middleware.html#middleware)
is as easy as creating a symfony service and using the `nfq_guzzle_config.middleware` tag, giving it an alias and
(optionally) a priority:

```yaml
app.middleware:
    class: App/Middleware
    tags:
        - { name: 'nfq_guzzle_config.middleware', alias: 'app_middleware', priority: 100 }
```

Middlewares are __not__ automatically used by your client (except global ones).
When registering your own clients with the bundle, you can explicitly list all enabled middleware.
The `middleware` attribute takes a space-delimited list of middleware aliases. In that case only the
specified middlewares (plus the global ones) will be registered for that client:

```yaml
services:
    app.client.foo:
        class: GuzzleHttp\Client
        tags:
            - { name: nfq_guzzle_config.client, alias: foo, middleware: 'app_middleware another_middleware' }
```

You can disable global middleware, by prefixing the global middleware alias with a `!` character.
Example below: `global_middleware` will be disabled, while `app_middleware` - enabled.

```yaml
services:
    app.client.foo:
        class: GuzzleHttp\Client
        tags:
            - { name: nfq_guzzle_config.client, alias: foo, middleware: '!global_middleware app_middleware' }
```

Defining a global middleware
----------------------------

If you wish, you can define your middleware as global. It means, that this middleware will be used by all your clients.
Set yours middlewares tags `global` value to `true`

```yaml
app.global_middleware:
    class: App/Middleware
    tags:
        - { name: 'nfq_guzzle_config.middleware', alias: 'global_middleware', priority: 100, global: true }
```

There are logger and retry global middlewares already available in the bundles configuration:
---------------------------------------------------------------------------------------------

### Logger middleware
```
Alias: guzzle_logger_middleware
Priority: 0
```

Logger middleware's objective is to provide a simple tool for logging Guzzle requests and responses.

Enabling request logging, you simply need to enable it in Symfony's configuration:

```yml
nfq_guzzle_config:
    logger:
        enabled: true
```

Using the advanced configuration, you may also configure your own logger, as long as it implements
the PSR-3 `LoggerInterface`:

```yml
nfq_guzzle_config:
    logger:
        enabled: true
        service: my_logger_service
```

You can configure the log format using the syntax described in [guzzlehttp/guzzle's documentation](https://github.com/guzzle/guzzle/blob/master/src/MessageFormatter.php#L12).
You may also use of the three levels described in the formatter: `clf` (Apache log format), `debug`, or `short`:

```yml
nfq_guzzle_config:
    logger:
        enabled: true
        format: debug
```

Default configuration:

```yml
nfq_guzzle_config:
    logger:
        enabled: true
        service: '@logger'
        format: debug
        level: debug # One of Psr\Log\LogLevel
```

### Retry middleware
```
Alias: guzzle_retry_middleware
Priority: 200
```

Retry middleware's objective is to retry failed requests.

```yml
nfq_guzzle_config:
    retry:
        enabled: true
        max_retries: 5
        delay: 100 # The number of milliseconds to delay before sending the retry request.
        logger: # Logger to log retries number. Default: NullLogger
        log_level: notice # Log level. Default: notice
```

---

Back to: [Base api manager](base_api_manager.md)

Next section: [FAQ](faq.md)
