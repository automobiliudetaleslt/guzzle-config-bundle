Client wrapper `BaseApiManager`
===============================

Base manager class is created for every configured client.
Managers services name pattern: `nfq_guzzle_config.client.{the alias you used in service tag}.manager`

```php
   // @var Nfq\Bundle\GuzzleConfigBundle\Manager\BaseApiManagerInterface $manager
   $manager = $this->get('nfq_guzzle_config.client.foo.manager');
```

---

Back to: [Usage with JMS response transformer](configuration_and_usage_jms.md)

Next section: [Middleware](middleware.md)
