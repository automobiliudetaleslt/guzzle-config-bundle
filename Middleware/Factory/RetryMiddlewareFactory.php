<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle\Middleware\Factory;

use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class RetryMiddlewareFactory
{
    /**
     * @param int $maxRetries
     * @param int $delay
     * @param LoggerInterface $logger
     * @param string $logLevel
     * @return callable
     */
    public static function create(
        int $maxRetries,
        int $delay,
        LoggerInterface $logger,
        string $logLevel = LogLevel::NOTICE
    ) {
        return Middleware::retry(self::retryDecider($maxRetries, $logger, $logLevel), self::getRetryDelay($delay));
    }

    /**
     * @param int $maxRetries
     * @param LoggerInterface $logger
     * @param string $logLevel
     * @return \Closure
     */
    private static function retryDecider(int $maxRetries, LoggerInterface $logger, string $logLevel = LogLevel::NOTICE)
    {
        return function (
            int $retries,
            Request $request,
            Response $response = null,
            \Throwable $exception = null
        ) use (
            $maxRetries,
            $logger,
            $logLevel
        ) {
            if (0 < $retries) {
                $logger->log($logLevel, \sprintf('Retrying request. Retry number: %d', $retries + 1));
            }

            // Limit the number of retries
            if (($retries + 1) >= $maxRetries) {
                return false;
            }

            // Retry connection exceptions
            if ($exception instanceof ConnectException) {
                return true;
            }

            // Server error
            if (null !== $response && $response->getStatusCode() >= 500) {
                return true;
            }

            return false;
        };
    }

    /**
     * @param int $delay The number of milliseconds to delay before sending the request.
     * @return \Closure
     */
    private static function getRetryDelay(int $delay)
    {
        return function ($numberOfRetries) use ($delay) {
            return $delay;
        };
    }
}
