<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle\DependencyInjection\CompilerPass;

use Nfq\Bundle\GuzzleConfigBundle\Factory\GuzzleClientFactory;
use Nfq\Bundle\GuzzleConfigBundle\Client\GuzzleClient;
use Nfq\Bundle\GuzzleConfigBundle\Manager\BaseApiManager;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

class GuzzleClientCompilerPass implements CompilerPassInterface
{
    const CLIENT_TAG = 'nfq_guzzle_config.client';

    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        foreach ($container->findTaggedServiceIds(self::CLIENT_TAG) as $id => $tags) {
            foreach ($tags as $attributes) {
                $clientDefinitionId = $this->getClientDefinitionId($attributes['alias']);

                if ($container->hasDefinition($clientDefinitionId)) {
                    throw new \RuntimeException('Duplicate aliases found while building nfq guzzle config client.');
                }

                $container->setDefinition(
                    $clientDefinitionId,
                    $this->createClientDefinition($id)
                );

                $container->setDefinition(
                    $this->getApiManagerDefinitionId($clientDefinitionId),
                    $this->createApiManagerDefinition($clientDefinitionId)
                );
            }
        }
    }

    /**
     * @param string $alias
     * @return string
     */
    private function getClientDefinitionId($alias)
    {
        return \sprintf('%s.%s', self::CLIENT_TAG, $alias);
    }

    /**
     * @param string $id
     * @return Definition
     */
    private function createClientDefinition($id)
    {
        $definition = new Definition(GuzzleClient::class, [new Reference($id)]);
        $definition->setFactory([new Reference(GuzzleClientFactory::class), 'getClient']);
        $definition->setPublic(true);

        return $definition;
    }

    /**
     * @param string $clientDefinitionId
     * @return string
     */
    private function getApiManagerDefinitionId($clientDefinitionId)
    {
        return \sprintf('%s.manager', $clientDefinitionId);
    }

    /**
     * @param string $clientDefinitionId
     * @return Definition
     */
    private function createApiManagerDefinition($clientDefinitionId)
    {
        $definition = new Definition(BaseApiManager::class, [new Reference($clientDefinitionId)]);
        $definition->setPublic(true);

        return $definition;
    }
}
