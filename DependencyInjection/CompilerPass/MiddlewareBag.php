<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle\DependencyInjection\CompilerPass;

class MiddlewareBag implements \Countable, \IteratorAggregate
{
    private const ID = 'id';
    private const ALIAS = 'alias';
    private const GLOBAL = 'global';
    private const PRIORITY = 'priority';

    /**
     * @var array
     */
    private $middlewares = [];

    /**
     * @param string $id
     * @param string $alias
     * @param bool $global
     * @param int $priority
     */
    public function add(string $id, string $alias, bool $global, int $priority)
    {
        $this->middlewares[$priority][] = [
            self::ID => $id,
            self::ALIAS => $alias,
            self::GLOBAL => $global,
            self::PRIORITY => $priority,
        ];
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->getPreparedMiddlewares();
    }

    /**
     * @return array
     */
    public function getGlobalMiddlewares(): array
    {
        $globalMiddlewares = [];
        foreach ($this->getPreparedMiddlewares() as $middleware) {
            if (true === $middleware[self::GLOBAL]) {
                $globalMiddlewares[] = $middleware;
            }
        }

        return $globalMiddlewares;
    }

    /**
     * @param array $whiteList
     * @param array $blackList
     * @return self
     */
    public function filterByWhiteBlackList(array $whiteList, array $blackList): self
    {
        $filteredBag = new self();
        foreach ($this->getPreparedMiddlewares() as $middleware) {
            $alias = $middleware[self::ALIAS];
            $global = $middleware[self::GLOBAL];

            if (true === $global) {
                if (\in_array($alias, $blackList)) {
                    continue;
                }

                $filteredBag->add(
                    $middleware[self::ID],
                    $middleware[self::ALIAS],
                    $middleware[self::GLOBAL],
                    $middleware[self::PRIORITY]
                );

                continue;
            }

            if (\in_array($alias, $blackList, true)) {
                throw new \LogicException('You cannot blacklist non global middleware.');
            }

            if (\in_array($alias, $whiteList, true)) {
                $filteredBag->add(
                    $middleware[self::ID],
                    $middleware[self::ALIAS],
                    $middleware[self::GLOBAL],
                    $middleware[self::PRIORITY]
                );
            }
        }

        return $filteredBag;
    }

    /**
     * @return array
     */
    public function getNonGlobalMiddlewares(): array
    {
        $nonGlobalMiddlewares = [];
        foreach ($this->getPreparedMiddlewares() as $middleware) {
            if (false === $middleware[self::GLOBAL]) {
                $nonGlobalMiddlewares[] = $middleware;
            }
        }

        return $nonGlobalMiddlewares;
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->getPreparedMiddlewares());
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return \count($this->middlewares);
    }

    /**
     * @return array
     */
    private function getPreparedMiddlewares(): array
    {
        if (!empty($this->middlewares)) {
            \krsort($this->middlewares);

            return \call_user_func_array('array_merge', $this->middlewares);
        }

        return $this->middlewares;
    }
}
