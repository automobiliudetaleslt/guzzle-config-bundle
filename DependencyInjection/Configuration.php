<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle\DependencyInjection;

use GuzzleHttp\MessageFormatter;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('nfq_guzzle_config');

        $rootNode
            ->children()
                ->arrayNode('logger')
                    ->canBeEnabled()
                    ->children()
                        ->scalarNode('service')
                            ->defaultNull()
                        ->end()
                        ->scalarNode('format')
                            ->beforeNormalization()
                                ->ifInArray(['clf', 'debug', 'short'])
                                ->then(
                                    function ($v) {
                                        return \constant('GuzzleHttp\MessageFormatter::' . \strtoupper($v));
                                    }
                                )
                                ->end()
                                ->defaultValue(MessageFormatter::DEBUG)
                        ->end()
                        ->scalarNode('level')
                            ->beforeNormalization()
                                ->ifInArray(
                                    [
                                        'emergency',
                                        'alert',
                                        'critical',
                                        'error',
                                        'warning',
                                        'notice',
                                        'info',
                                        'debug',
                                    ]
                                )
                                ->then(
                                    function ($v) {
                                        return \constant('Psr\Log\LogLevel::' . \strtoupper($v));
                                    }
                                )
                            ->end()
                            ->defaultValue('debug')
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('retry')
                    ->canBeEnabled()
                    ->children()
                        ->integerNode('max_retries')
                            ->defaultValue(3)
                        ->end()
                        ->integerNode('delay')
                            ->defaultValue(100)
                        ->end()
                        ->scalarNode('logger')
                            ->defaultValue('nfq_guzzle_config.logger.null')
                        ->end()
                        ->scalarNode('log_level')
                            ->beforeNormalization()
                                ->ifInArray(
                                    [
                                        'emergency',
                                        'alert',
                                        'critical',
                                        'error',
                                        'warning',
                                        'notice',
                                        'info',
                                        'debug',
                                    ]
                                )
                                ->then(
                                    function ($v) {
                                        return \constant('Psr\Log\LogLevel::' . \strtoupper($v));
                                    }
                                )
                                ->end()
                            ->defaultValue('notice')
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
