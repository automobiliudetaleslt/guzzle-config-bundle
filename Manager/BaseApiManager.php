<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle\Manager;

use GuzzleHttp\Command\CommandInterface;
use GuzzleHttp\Command\ResultInterface;
use Nfq\Bundle\GuzzleConfigBundle\Client\GuzzleClient;

class BaseApiManager implements BaseApiManagerInterface
{
    /**
     * @var GuzzleClient
     */
    protected $client;

    /**
     * @param GuzzleClient $client
     */
    public function __construct(GuzzleClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return ResultInterface|mixed
     */
    public function __call(string $name, array $arguments = [])
    {
        return $this->call($name, $arguments);
    }

    /**
     * {@inheritdoc}
     */
    public function call(string $commandName, array $arguments = [])
    {
        return $this->getResponse(
            $this->getCommand($commandName, $arguments)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getCommand(string $commandName, array $arguments = []): CommandInterface
    {
        return $this->client->getCommand($commandName, $arguments);
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse(CommandInterface $command)
    {
        try {
            return $this->client->execute($command);
        } catch (\Exception $exception) {
            return $this->handleResponseException($exception, $command);
        }
    }

    /**
     * @param \Exception $exception
     * @param CommandInterface $command
     * @return mixed
     */
    protected function handleResponseException(\Exception $exception, CommandInterface $command)
    {
        return false;
    }
}
